(function( ng, app ){
    "use strict";
    app.controller(
        "user.UserinfoController",
        ['$scope', 'requestContext', '$http', '$location', 'appVarService',
        function( $scope, requestContext, $http, $location, appVarService ) {
            // --- Define Controller Methods. ------------------- //
            appVarService.getData().then(function(data){
                $scope.Yii = data;
                $scope.language = data.user.language;
                $scope.loggedInUser = data.user;
                loadData();
                
            });

            // I apply the remote data to the local view model.


            // --- Define Scope Methods. ------------------------ //
            // --- Define Controller Variables. ----------------- //


            // Get the render context local to this controller (and relevant params).
            var renderContext = requestContext.getRenderContext("withleftnavbarupcommingevents");

            
            // --- Define Scope Variables. ---------------------- //


            // The subview indicates which view is going to be rendered on the page.
            $scope.subview = renderContext.getNextSection();


            // --- Bind To Scope Events. ------------------------ //


            // I handle changes to the request context.
            $scope.$on(
                "requestContextChanged",
                function() {
                    // Make sure this change is relevant to this controller.
                    if ( ! renderContext.isChangeRelevant() ) {
                        return;
                    }
                    // Update the view that is being rendered.
                    $scope.subview = renderContext.getNextSection();
                }
            );
            
            $scope.$on('changeContextUser', function(){
            	loadData();
            });
            $scope.$on('userChanged', function(){
            	loadData();
            });            

            // --- Initialize. ---------------------------------- //

            function loadData(){
                if($scope.Yii.context_user_id){
                    $scope.showNotify(sitewide_lables.loading);
                    $http.get('user/user/'+$scope.Yii.context_user_id)
	                    .success(function(data) {
	                        $scope.user = data.user;
	                    })
	                    .finally(function(){
	                        $scope.hideNotify();
	                    });
                }
                else{
                    $scope.user = null;
                }
            }
        }]
    );

})( angular, AppinuxCare );