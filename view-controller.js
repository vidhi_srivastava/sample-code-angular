(function( ng, app ){

    "use strict";

    app.controller(
        "user.ViewController",
        ['$scope', 'requestContext', '$http', '$location', 'appVarService',
        function( $scope, requestContext, $http, $location, appVarService ) {
            // --- Define Controller Methods. ------------------- //
            appVarService.getData().then(function(data){
                $scope.Yii = data;
                if(!(($scope.id && ($scope.Yii.user.id == $scope.id || $scope.Yii.user.admin==true)) || (!$scope.id && $scope.Yii.user.admin==true))){
                    $location.path('/notauthorized');
                    return;
                }
                
                $scope.language = data.user.language;
                $scope.loggedInUser = data.user;
                loadData();
                
            });

            // I apply the remote data to the local view model.
            $scope.id = requestContext.getParam( "id" );


            // --- Define Scope Methods. ------------------------ //
            $scope.deleteUser=function(){
                if($scope.id){
                    $http['delete']('/user/user/'+$scope.id)
                        .success(function(){
                            $location.path('/user');
                        });
                }
            };

            // --- Define Controller Variables. ----------------- //


            // Get the render context local to this controller (and relevant params).
            var renderContext = requestContext.getRenderContext( "withleftnavbarupcommingevents.user.view", 'id' );

            
            // --- Define Scope Variables. ---------------------- //


            // The subview indicates which view is going to be rendered on the page.
            $scope.subview = renderContext.getNextSection();


            // --- Bind To Scope Events. ------------------------ //


            // I handle changes to the request context.
            $scope.$on(
                "requestContextChanged",
                function() {

                    // Make sure this change is relevant to this controller.
                    if ( ! renderContext.isChangeRelevant() ) {
                        return;
                    }
                    $scope.id = requestContext.getParam( "id" );
                    if(!(($scope.id && ($scope.Yii.user.id == $scope.id || $scope.Yii.user.admin==true)) || (!$scope.id && $scope.Yii.user.admin==true))){
                        $location.path('/notauthorized');
                        return;
                    }
                    
                    if ( requestContext.haveParamsChanged( [ "id"] ) ) {
                        loadData();
                    }
                    // Update the view that is being rendered.
                    $scope.subview = renderContext.getNextSection();

                }
            );


            // --- Initialize. ---------------------------------- //

            function loadData(){
                if($scope.id){
                    $scope.showNotify(user_labels.loading);
                    $http.get('user/user/'+$scope.id)
	                    .success(function(data) {
	                        data.user.create_at = Date.parse(data.user.create_at);
	                        data.user.lastvisit_at = Date.parse(data.user.lastvisit_at);
	                        $scope.user = data.user;
	                    })
	                    .finally(function(){
	                        $scope.hideNotify();
	                    });
                }
                else{
                    $scope.user = null;
                }
            }
        }]
    );

})( angular, AppinuxCare );