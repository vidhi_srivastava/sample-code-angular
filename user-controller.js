(function( ng, app ){
    "use strict";
    app.controller(
        "user.UserController",
        ['$scope', 'requestContext', '$http', 'appVarService',
        function( $scope, requestContext, $http, appVarService ) {
            appVarService.getData().then(function(data){
                $scope.language = data.user.language;
            });

            // --- Define Controller Methods. ------------------- //
            // ...
            // --- Define Scope Methods. ------------------------ //
            // --- Define Controller Variables. ----------------- //


            // Get the render context local to this controller (and relevant params).
            var renderContext = requestContext.getRenderContext( "withleftnavbarupcommingevents.user" );
            
            // --- Define Scope Variables. ---------------------- //

            // The subview indicates which view is going to be rendered on the page.
            $scope.subview = renderContext.getNextSection();

            // --- Bind To Scope Events. ------------------------ //

            // I handle changes to the request context.
            $scope.$on(
                "requestContextChanged",
                function() {

                    // Make sure this change is relevant to this controller.
                    if ( ! renderContext.isChangeRelevant() ) {
                        return;
                    }

                    // Update the view that is being rendered.
                    $scope.subview = renderContext.getNextSection();

                }
            );

            $scope.$on(
                "userLanguageChanged",
                function() {
                    // Make sure this change is relevant to this controller.
                    if ( ! renderContext.isChangeRelevant() ) {
                        return;
                    }
                    appVarService.getData().then(function(data){
                        $scope.language = data.user.language;
                        // Update the view that is being rendered.
                        $scope.subview = renderContext.getNextSection();
                    });

                }
            );
            // --- Initialize. ---------------------------------- //
        }]
    );
})(angular, AppinuxCare);