(function( ng, app ){

	"use strict";

	app.controller(
		"user.IndexController",
        ['$scope', 'requestContext', '$http', 'appVarService',
		function( $scope, requestContext, $http, appVarService ) {
            appVarService.getData().then(function(data){
                $scope.language = data.user.language;
                $scope.loggedInUser = data.user;
                tagMagnager([]);

            });

            $scope.toggleSearchForm = function(){
                jQuery('#div_user_advance_search').slideToggle();
            };
            
            $scope.searchUser=function(){
                $scope.searchData = jQuery('#frm_user_search').serialize();
                loadData(1);
            };

            $scope.clearSearchForm = function(){
                $scope.searchData = null;
                $('#frm_user_search')[0].reset();
                jQuery('#User_tags').tagsManager('empty');
                loadData(1);
            };

			function tagMagnager(tags){
				$scope.tagManager = jQuery('#User_tags').tagsManager({
					deleteTagsOnBackspace: false,
					prefilled:tags,
					typeaheadAjaxPolling: true,
					typeahead: true,
					typeaheadSource: function (query, process){
					    if(query){
							return $.getJSON('/user/user/tags',{'q':query}, function(result){
								process(result.tags);
							});
						}
					}
				});				
			}

            function loadData(currentPage){
                if(typeof currentPage == 'undefined' || currentPage < 1){
                    currentPage =1;
                }
                $scope.showNotify(user_labels.loading);
                var url = 'user/user/list/page/'+currentPage;
                if($scope.searchData){
                    url += '?'+ $scope.searchData; 
                }
                $http.get(url)
	                .success(function(data) {
	                    $scope.users = data.users;
	                    $scope.totalPages = data.pagination.totalPages*1;
	                    $scope.currentPage = data.pagination.currentPage*1;
	                })
	                .finally(function(){
	                    $scope.hideNotify();
	                });
            }

			// Get the render context local to this controller (and relevant params).
			var renderContext = requestContext.getRenderContext( "withleftnavbarupcommingevents.user.index" );
			// The subview indicates which view is going to be rendered on the page.
			$scope.subview = renderContext.getNextSection();

			// I handle changes to the request context.
			$scope.$on(
				"requestContextChanged",
				function() {

					// Make sure this change is relevant to this controller.
					if ( ! renderContext.isChangeRelevant() ) {
						return;
					}

					// Update the view that is being rendered.
					$scope.subview = renderContext.getNextSection();
				}
			);

            $scope.$watch('currentPage', function(newVal, oldVal){
                if(typeof oldVal != 'undefined' && typeof newVal != 'undefined' && oldVal != newVal){
                    loadData(newVal);
                }
            });

            loadData();
		}]
	);
})( angular, AppinuxCare );