(function( ng, app ){

    "use strict";

    app.controller(
        "user.ChangePasswordController",
        ['$scope', 'requestContext', '$http', '$location', 'appVarService',
        function( $scope, requestContext, $http, $location, appVarService ) {
            appVarService.getData().then(function(data){
                $scope.language = data.user.language;
                $scope.loggedInUser = data.user;
                loadData();
            });

            // --- Define Controller Methods. ------------------- //

			function loadData(){
				$scope.showNotify(user_labels.loading);
				$http({
					url:'user/user/genpwd',
					method:'GET'
				})
					.success(function(data){
						$scope.pwds = data.pwds;
					})
					.finally(function(){
						$scope.hideNotify();
					});
			}
            // ...
            $scope.id = requestContext.getParam( "id" );

            // --- Define Scope Methods. ------------------------ //
            $scope.changepassword=function(){
                $scope.showNotify(user_labels.saving);
                var url, data, method;
                method = 'PUT';
                if($scope.id == $scope.loggedInUser.id){
                    url = 'user/user/changePassword/' + $scope.id;
                    data = {UserChangePassword:$scope.UserChangePassword};
                }
                else{
                    url = 'user/user/changeOtherUserPassword/' + $scope.id;
                    data = {User:{password:$scope.UserChangePassword.password, confirmpassword:$scope.UserChangePassword.verifyPassword}};
                }
                $http({
                    method:method,
                    url:url,
                    data:data
                })
	                .success( function(data){
	                    $location.path('/user/view/'+$scope.id);
	                })
	                .error(function(data, status){
						if(status == 400){
	                        var fields = ['oldPassword', 'password', 'verifyPassword'];
	                        $scope.showFormErrors(fields, data.errors[0]);
						}
	                })
	                .finally(function(){
	                    $('.btn.save').removeAttr("disabled");
	                    $scope.hideNotify();
	                });
            };

            // --- Define Controller Variables. ----------------- //
            $scope.UserChangePassword = null;

            // Get the render context local to this controller (and relevant params).
            var renderContext = requestContext.getRenderContext( "withleftnavbarupcommingevents.user.changepassword", 'id');
            
            // --- Define Scope Variables. ---------------------- //


            // The subview indicates which view is going to be rendered on the page.
            $scope.subview = renderContext.getNextSection();


            // --- Bind To Scope Events. ------------------------ //


            // I handle changes to the request context.
            $scope.$on(
                "requestContextChanged",
                function() {

                    // Make sure this change is relevant to this controller.
                    if ( ! renderContext.isChangeRelevant() ) {

                        return;

                    }
                    $scope.id = requestContext.getParam( "id" );
                    // Update the view that is being rendered.
                    $scope.subview = renderContext.getNextSection();

                }
            );


            // --- Initialize. ---------------------------------- //
            $scope.UserChangePassword={};

        }]
    );

})( angular, AppinuxCare );