(function( ng, app ){
    "use strict";
    app.controller(
        "user.EditController",
        ['$scope', 'requestContext', '$http', '$location', 'appVarService',
        function( $scope, requestContext, $http, $location, appVarService ) {
            // --- Define Controller Methods. ------------------- //
            appVarService.getData().then(function(data){
                if(!(($scope.id && ($scope.Yii.user.id == $scope.id || $scope.Yii.user.admin==true)) || (!$scope.id && $scope.Yii.user.admin==true))){
                    $location.path('/notauthorized');
                    return;
                }
                $scope.language = data.user.language;
                $scope.preferred_date_format = data.user.preferred_date_format;
                $scope.loggedInUser = data.user;
                if(data.user.admin && $('label[for="cprnr"]').hasClass('required')){
                    $('label.required[for="cprnr"]').removeClass();
                }
                else if(!data.user.admin){
                    $('label[for="cprnr"]').addClass('required');
                }
                loadData();
            });
            
			$scope.saveButton = true;

            // I apply the remote data to the local view model.
            $scope.id = requestContext.getParam("id");
            
            // use only numeric characters
            $('#cprnr').die().live('keyup',function(){
                $(this).val($(this).val().replace(/[^0-9]/g, '').substr(0,10));
            });

            // --- Define Scope Methods. ------------------------ //
            $scope.save=function(){
                $scope.showNotify(user_labels.saving);
                var url = 'user/user/';
                var method = 'POST';
                $scope.user.tags = [];
                $('.control-group.tags span.tm-tag>span').each(function(){
                	$scope.user.tags.push($(this).text())
                });
                if($scope.id){
                    url+=$scope.id;
                    method = 'PUT';
                }
                
                $http({
                    method:method,
                    url:url,
                    data:{User:$scope.user}
                })
	                .success( function(data){
	                    appVarService.broadcastEventMarkAdDone();
	                    if($scope.Yii.user.id == $scope.id){

	                        //If theme changed
	                        if(data.themeChanged == '1'){
                        		var theme = data.user.theme;
                        		if($('link[name="theme-'+theme+'"]').size() < 1){
                        			//CSS Not yet loaded
									LazyLoad.css($scope.Yii.app.themes[theme]['css'], function () {}, null, null, {name:'theme-'+theme});
                        		}
                        		else{
									//CSS already loaded
									$('link[name^="theme-"]').attr('disabled', true);
									$('link[name="theme-'+theme+'"]').attr('disabled', false);
                        		}
                        		
							}
	                        
	                        //If same user before redirect, reload appVarService forcefully
	                        appVarService.getData(true).then(function(data){
	                            if($scope.language != data.user.language || $scope.preferred_date_format != data.user.preferred_date_format){
	                                appVarService.broadcastUserLanguageChanged();
	                            }
	                            else{
	                                appVarService.broadcastUserChanged();
	                            }
	                            $location.path('/user/view/'+data.user.id);
	                        });
	                    }
	                    else{
	                        $location.path('/user/view/'+data.user.id);
	                    }
	                })
	                .error(function(data, status){
                		if(status==400){
	                        var fields = ['username', 'password', 'confirmpassword', 'email',
	                            'superuser', 'status', 'first_name', 'last_name',
	                            'language', 'preferred_date_format', 'mobile',
	                            'address', 'zipcode', 'city', 'dob', 'sex', 'consent',
	                            'alrernate_email', 'event_invitation', 'cprnr', 'theme', 'left_key', 'browser_notification'];
	                        $scope.showFormErrors(fields, data.errors[0]);
                		}
	                })
	                .finally(function(){
	                    $('.btn.save').removeAttr("disabled");
	                    $scope.hideNotify();
	                });
            };

            $scope.removeProfileImage=function(){
                $scope.showNotify(user_labels.loading);
                $http({method:'GET', url:'user/user/removeProfileImage/'+$scope.id})
                    .success(function(data){
                        $scope.user.profileImage = 0;
                        $scope.updateProfileImage(data.profile_image_timestamp);
                    })
                    .finally(function(){
                        $scope.hideNotify();
                    });
            };
            // --- Define Controller Variables. ----------------- //


            // Get the render context local to this controller (and relevant params).
            var renderContext = requestContext.getRenderContext( "withleftnavbarupcommingevents.user.edit", 'id' );

            
            // --- Define Scope Variables. ---------------------- //


            // The subview indicates which view is going to be rendered on the page.
            $scope.subview = renderContext.getNextSection();


            // --- Bind To Scope Events. ------------------------ //


            // I handle changes to the request context.
            $scope.$on(
                "requestContextChanged",
                function() {

                    // Make sure this change is relevant to this controller.
                    if ( ! renderContext.isChangeRelevant() ) {
                        return;
                    }
                    $scope.id = requestContext.getParam( "id" );
                    if(!(($scope.id && ($scope.Yii.user.id == $scope.id || $scope.Yii.user.admin==true)) || (!$scope.id && $scope.Yii.user.admin==true))){
                        $location.path('/notauthorized');
                        return;
                    }

                    if ( requestContext.haveParamsChanged( [ "id"] ) ) {
                    	jQuery('#tags').tagsManager('empty');
                    	jQuery('#tags').remove();
                    	$('#addTag').before('<input type="text" class="tm-input" size="20" maxlength="255" autocomplete="off" name="User[tags]" id="tags"/>');
                        loadData();
                        //setupProfileImageUpload();
                    }
                    // Update the view that is being rendered.
                    $scope.subview = renderContext.getNextSection();

                }
            );


            // --- Initialize. ---------------------------------- //

            function loadData(){
                if($scope.id){
                    $scope.showNotify(user_labels.loading);
                    $http.get('user/user/'+$scope.id)
	                    .success(function(data) {
	                        if(data.user.dob && data.user.dob!= '0000-00-00'){
	                            data.user.dob = Date.parse(data.user.dob).toString($scope.Yii.user.preferred_date_format);
	                        }
	                        $scope.user = data.user;
						    tagMagnager($scope.user.tags);
	                    })
	                    .finally(function(){
	                        $scope.hideNotify();
	                    });
                }
                else{
                	$scope.showNotify(user_labels.loading);
					$http({
						url:'user/user/genpwd',
						method:'GET'
					})
						.success(function(data){
							$scope.pwds = data.pwds;
						})
						.finally(function(){
							$scope.hideNotify();
						});
                	
                    $scope.user = {
                    	superuser:0,
                    	status:0,
                    	preferred_date_format:'',
                    	change_password:1,
                    	tags:[],
                    	left_key:0,
                    	browser_notification:$scope.Yii.app.user.browserNotification,
                    	consent:1
                    };
                    tagMagnager($scope.user.tags);
                }
            }
            
			function tagMagnager(tags){
				$scope.tagManager = jQuery('#tags').tagsManager({
					deleteTagsOnBackspace: false,
					prefilled:tags,
					typeaheadAjaxPolling: true,
					typeahead: true,
					typeaheadSource: function (query, process){
					    if(query){
							return $.getJSON('/user/user/tags',{'q':query}, function(result){
								process(result.tags);
							});
						}
					}
				});				
			}

            $scope.updateProfileImage = function (profile_image_timestamp){
				if ($scope.id == $scope.Yii.user.id) {
                	$scope.Yii.user.profile_image_timestamp = profile_image_timestamp;
				}
                $scope.user.profile_image_timestamp = profile_image_timestamp;
                if(!$scope.$$phase){
                    $scope.$apply();
                }
            };
            
            $scope.addTag = function(){
			    jQuery('#tags').tagsManager('pushTag', jQuery('#tags').val());
            };

            function fineUploader(){
                if($('#thumbnail-fine-uploader').size()<1){
                    return;
                }
                $('#thumbnail-fine-uploader').fineUploader({
                    request: {
                        endpoint: '/user/user/upload/' + $scope.id
                    },
                    multiple: false,
                    validation: {
                        allowedExtensions: ['jpg'],
                        sizeLimit: 10240000 // 50 kB = 50 * 1024 bytes
                    },
                    text:user_labels,
                    button_info_text:$('#thumbnail-fine-uploader').data('button_info_text'),
                    debug: false
                }).on('complete', function (event, id, fileName, responseJSON) {
                    $('.qq-upload-button').show();
                    if (responseJSON.success) {
                        $scope.user.profileImage = 1;
                        $scope.updateProfileImage(responseJSON.profile_image_timestamp);
                    }
                }).on('upload', function(){
                    $('.qq-upload-button').hide();
                }).on('cancel', function(){
                    $('.qq-upload-button').show();
                }).on('error', function(){
                    $('.qq-upload-button').show();
                });
            }
            fineUploader();
        }]
    );
})( angular, AppinuxCare );
