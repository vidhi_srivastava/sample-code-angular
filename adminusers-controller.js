(function( ng, app){
	"use strict";
	app.controller(
		"user.AdminUsersController",
        ['$scope', 'requestContext', '$http', 'appVarService',
		function( $scope, requestContext, $http, appVarService ) {
			var renderContext;
            appVarService.getData().then(function(data){
                $scope.language = data.user.language;
                $scope.loggedInUser = data.user;
                
				// Get the render context local to this controller (and relevant params).
                renderContext = requestContext.getRenderContext( "withleftnavbarupcommingevents.user.adminusers");

				// The subview indicates which view is going to be rendered on the page.
                $scope.subview = renderContext.getNextSection();

                loadData();
            });

            function loadData(currentPage){
                $scope.showNotify(user_labels.loading);
                var url = 'user/user/getAdmins';
                $http.get(url)
	                .success(function(data) {
	                    $scope.users = data.users;
	                })
	                .finally(function(){
	                    $scope.hideNotify();
	                });
            }

			// I handle changes to the request context.
			$scope.$on(
				"requestContextChanged",
				function(){

					// Make sure this change is relevant to this controller.
					if (!renderContext.isChangeRelevant()){
						return;
					}

					// Update the view that is being rendered.
					$scope.subview = renderContext.getNextSection();
				}
			);
		}]
	);
})(angular, AppinuxCare);