(function( ng, app ){

    "use strict";

    app.controller(
        "user.SimilarController",
        ['$scope', 'requestContext', '$http', '$location', 'appVarService',
        function( $scope, requestContext, $http, $location, appVarService ) {
            appVarService.getData().then(function(data){
                $scope.language = data.user.language;
                $scope.loggedInUser = data.user;
            });

            // --- Define Controller Methods. ------------------- //


            // ...
            $scope.term = requestContext.getParam( "term" );
            function loadData(){
                $scope.showNotify(user_labels.loading);
                $http.get('user/user/similar/q/'+$scope.term)
	                .success(function(data) {
	                    $scope.users = data.users;
	                })
	                .finally(function(){
	                    $scope.hideNotify();
	                });
                
            }
            
            $scope.setContexUser = function(user){
                $http.post('/site/setNewContextUserId', {new_context_user_id:user.id})
                    .success(function(){
                        $scope.Yii.context_user_name = user.first_name+' '+user.last_name;
                        $scope.Yii.context_user_id = user.id;
                        appVarService.changeContextUser(user.id, user.first_name+' '+user.last_name);
                    })
                    .error(function(){});
            };
            // --- Define Scope Methods. ------------------------ //

            $scope.alerts = [];
            $scope.addAlert = function(msg){
                $scope.alerts.push({type: 'error', content: msg});
            };
            
            // --- Define Controller Variables. ----------------- //
            $scope.UserChangePassword = null;

            // Get the render context local to this controller (and relevant params).
            var renderContext = requestContext.getRenderContext( "withleftnavbarupcommingevents.user.similar", 'term');
            
            // --- Define Scope Variables. ---------------------- //


            // The subview indicates which view is going to be rendered on the page.
            $scope.subview = renderContext.getNextSection();


            // --- Bind To Scope Events. ------------------------ //


            // I handle changes to the request context.
            $scope.$on(
                "requestContextChanged",
                function() {

                    // Make sure this change is relevant to this controller.
                    if ( ! renderContext.isChangeRelevant() ) {
                        return;
                    }
                    $scope.term = requestContext.getParam("term");
                    if (requestContext.haveParamsChanged(["term"])){
                        loadData();
                    }                    
                    // Update the view that is being rendered.
                    $scope.subview = renderContext.getNextSection();

                }
            );


            // --- Initialize. ---------------------------------- //
            loadData();

        }]
    );

})(angular, AppinuxCare);